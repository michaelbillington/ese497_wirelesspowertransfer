// Char to int: subtract 48;

#include "Math.h";
int isFB = 0;                     //send something to signal arrival of feedback
int FBcurr = 0;                   //current feedback number
int newZero = 0;                  //set receiver at degree 0 and measure feedback, this becomes "new zero" baseline...this will mostly likely need to be changed manually
const int numAntennas = 2;        //manually input number of antennas
const int numPhases = 2;          //manually input how many phases each antenna has
const int cushion = 10;           //manually set the range that is good enough...e.g. if the best (newZero) is 15, and cushion is 10, the "good enough" will be < 26
const int FBpin = A0;             //if feedback is being wired in
double ds[] = {0, 1};             //manually input the location of the antennas
double ls[] = {0, 1};             //manually input the length of the phases
const int dnum = sizeof(ds) / sizeof(double);   //computes how many antennas there are
const int lnum = sizeof(ls) / sizeof(double);   //computes how many phases each antenna has
int pin[dnum][lnum];              //
int pinCount = 3;                 //the first pin that you're using on the Arduino board
const int sets = pow(lnum, dnum); //the number of combinations
int M[dnum][2][sets];             //the matrix used to calculated the theta array
int v[dnum];                      //matrix used to help M matrix calculate the theta array
int n = 0;                        //
String x = "";                    //
double dlist[dnum];               //array of d values
double llist[dnum];               //array of l values
double thetas[sets];              //array of potential theta values based on the input ds and ls
double thisTheta = 0;             //initial value for feedback finding
int holdBestVal[3];               //feedback comparison helper array
int holdTheta[3];                 //another feedback comparison helper array
int holdBest[3];                  //a third feedback comparison helper array
int FBpos = 0;                    //initial value for declared variables
int FBneg = 0;                    //
int thisD = 0;                    //
int thisL = 0;                    //
int count = 0;                    //
int findTheta = 0;                //

String convert(int a, int b, int c) {     //converts 
  double power = pow(b, c - 1) - .01;     // delta 0.01 added for correct rounding
  int d = min((int)(a / power), b - 1);
  int r = a - (d * power);
  String x = (String)d;
  if (c - 1 > 0) {
    x = x + convert(r, b, c - 1);
  }
  else {
    return x;
  }
}

int sizer(double x[]) {                   //returns size of an array
  return (sizeof(x) / sizeof(double));
}

double meanX(double x[], int n) {         //calculates the mean of an array
  double sum = 0;
  for (int i = 0; i < n; i++) {
    sum = sum + x[i];
  }
  return (sum / n);
}

double stdD(double x[], int n) {          //calculates the standard deviation of an array
  double meanie = meanX(x, n);
  double sum = 0;
  for (int i = 0; i < n; i++) {
    sum += pow(x[i] - meanie, 2);
  }
  return (sqrt( sum / (n - 1) ) );
}

double cov(double x[], double y[], int n) {     //calculates the covariance of 2 arrays
  double sum = 0;
  for (int i = 0; i < n; i++) {
    sum += (x[i] - meanX(x, n)) * (y[i] - meanX(y, n));
  }
  return ( sum / (n - 1) );
}

double corr(double x[], double y[], int n) {    //calculates the correlation between 2 arrays
  double thingOne = stdD(y, n);
  if (thingOne == 0) {
    thingOne = 1;
  }
  return ( cov(x, y, n) / (stdD(x, n) * thingOne) );
}

double slopeX(double x[], double y[], int n) {          //calculates the slope between  the values in 2 arrays...used for calculating the theta array
  return (corr(x, y, n) * stdD(y, n) / stdD(x, n));
}

double thetaX(double x[], double y[], int n) {          //finds the theta value associated with each combination of phases for the antennas
  double slope = slopeX(x, y, n);
  return (atan(slope));
}

void setup() {
  Serial.begin(9600);                     //necessary for using the Serial Monitor
  holdTheta[0] = thisTheta;               
  holdTheta[0] = 0;
  for (int i = 2; i < 6; i++) {           //initialize all the pins you're using for output
    pinMode(i, OUTPUT);
  }
  for (int k = 0; k < sets; k++) {        //create the M and v arrays
    for (int j = 0; j < 2; j++) {
      for (int i = 0; i < dnum; i++) {
        M[i][j][k] = 0;
        v[i] = 0;
      }
    }
  }
  for (int k = 0; k < sets; k++) {        //fill the M array with d values
    for (int i = 0; i < dnum; i++) {
      M[i][0][k] = ds[i];
    }
  }
  for (int k = 0; k < sets; k++) {        //fill the M array with l values
    for (int i = 0; i < dnum; i++) {
      M[i][1][k] = ls[v[i]];
    }
    n++;
    x = "";
    x = convert(n, lnum, dnum);
    for (int j = 0; j < dnum; j++) {
      v[j] = (int)x[j] - 48;
    }
  }
  for (int i = 0; i < sets; i++) {
    thetas[i] = 0;
  }
  for (int i = 0; i < dnum; i++) {
    dlist[i] = 0;
    llist[i] = 0;
  }
  for (int k = 0; k < sets; k++) {
    double summing = 0;
    for (int i = 0; i < dnum; i++) {
      dlist[i] = M[i][0][k];
      llist[i] = M[i][1][k];
      summing += dlist[i];
    }
    thetas[k] = thetaX(dlist, llist, dnum);   //writes the theta array using dlist and llist
    //Serial.println(180 / PI * thetas[k]);
  }
  //Serial.println();
  for (int i = 0; i < dnum; i++) {        //assigns the pins to a specific phase on a specific antenna
    for (int j = 0; j < lnum; j++) {
      pin[i][j] = pinCount;
      pinCount++;
    }
  }
  for (int i = 0; i < dnum; i++) {        //set phase for 0 degrees...so the shortest phase for every antenna is turned on
    digitalWrite(pin[i][0], HIGH);
  }
  delay(500);                             //unnecessary for manual feedback but may be necessary in a more developed physical system
  FBcurr = FB();                          //find the current feedback for 0 degrees
}

void loop() {
  int thetaPosIndex = findThetaPos(thisTheta);    //finds the angle 1 higher than the current angle
  pins(thetaPosIndex);                            //activates the pins for that angle
  delay(500);                                     //again, potentially unnecessary
  FBpos = FB();                                   //find feedback for that angle
  holdBestVal[1] = FBpos;                         //helper array stores feedback for that angle
  int thetaNegIndex = findThetaNeg(thisTheta);    //finds the angle 1 lower than the current angle
  pins(thetaNegIndex);                            //activates the pins for that angle
  delay(500);                                     //
  FBneg = FB();                                   //find feedback for that angle
  holdBestVal[2] = FBneg;                         //helper array stores feedback for that angle 
  holdBestVal[0] = FBcurr;
  holdBestVal[1] = FBpos;
  holdBestVal[2] = FBneg;
  int findBest = min(min(FBpos, FBneg), FBcurr);  //find the minimum of the current feedback, positive angle feedback, and negative angle feedback
  //Serial.print("findBest ");
  //Serial.println(findBest);
  for (int i = 0; i < 3; i++) {
    if (holdBestVal[i] == findBest) {             //find the theta value that corresponds to the best/minimum feedback
      //Serial.println("in findBest loop");
      //Serial.print("holdBest ");
      //Serial.println(holdBest[i]);
      findTheta = holdBest[i];
      //Serial.print("findTheta ");
      //Serial.println(findTheta);
      //Serial.print("thisTheta ");
      //Serial.println(thisTheta);
      //Serial.print("thetas[findTheta] ");
      //Serial.println(thetas[findTheta]);
      thisTheta = thetas[findTheta];              //set the current theta to be that theta value
    }
  }
  //Serial.println("about to do pins of thisTheta");
  //Serial.print("thisTheta ");
  //Serial.println(thisTheta);
  pins(findTheta);                                //activate the pins for the new current theta value
  delay(500);                                     //
  FBcurr = findBest;                              
  while ((FBcurr < (newZero + cushion)) && (count < 5)) {     //while the feedback is less than newZero + cushion and the while loop iterations are below some threshold...
    FBcurr = FB();                                            //continuously check the feedback
    count++;
  }
  count = 0;
}

int FB() {                      //method that finds the feedback
  int FB = 0;                   //since the feedback in our design was manual, we just had a loop that waited for input from the Serial Monitor
  while (FB == 0) {             //this loop will need to be changed is the feedback is input in some other way
    FB = Serial.parseInt();
  }
  //Serial.print("FB ");
  //Serial.println(FB);
  return FB;
}

int findThetaPos(double thisTheta) {    //this method finds the closest value in the theta array that is one higher than the current theta value
  //Serial.println("In pos Theta");
  double curBest = 10000;
  int curBestIndex = findTheta;
  double holder = 0;
  double curCheck = 0;
  for (int i = 0; i < sets; i++) {      //check every theta value to see if it's the best one
    holder = thetas[i];
    curCheck = holder - thisTheta;
    if (curCheck > 0) {
      if (curCheck < curBest) {
        curBest = curCheck;             //save the best value
        curBestIndex = i;
      }
    }
  }
  holdTheta[1] = curBestIndex;          //at the end, save the index of the best value
  holdBest[1] = curBestIndex;
  //Serial.print("curBestIndex ");
  //Serial.println(curBestIndex);
  return curBestIndex;                  //return the index of the best value
}

int findThetaNeg(double thisTheta) {    //works the same way as the findThetaPos method except it finds the closest theta value one lower than the current theta value
  //Serial.println("In neg Theta");
  double curBestNeg = -10000.00;
  int curBestNegIndex = findTheta;
  double holder = 0;
  double curCheck = 0;
  for (int i = 0; i < sets; i++) {
    holder = thetas[i];
    curCheck = holder - thisTheta;
    if (curCheck < 0.00) {
      if (curCheck > curBestNeg) {
        curBestNeg = curCheck;
        curBestNegIndex = i;
      }
    }
  }
  //Serial.print("curBestNeg ");
  //Serial.println(curBestNeg);
  holdTheta[2] = curBestNegIndex;
  holdBest[2] = curBestNegIndex;
  //Serial.print("curBestNegIndex ");
  //Serial.println(curBestNegIndex);
  return curBestNegIndex;               //return the index of the best value
}

void pins(int index) {                  //activates the pins for a certain theta value 
  for (int i = 0; i < dnum; i++) {      //writes every pin to low first
    for (int j = 0; j < lnum; j++) {
      digitalWrite(pin[i][j], LOW);
    }
  }
  for (int i = 0; i < dnum; i++) {            //for each antenna
    int curD = M[i][0][index];
    for (int i = 0; i < dnum; i++) {          //find the ds value 
      if (curD == ds[i]) {
        thisD = i;
      }
    }
    int curL = M[i][1][index];
    for (int i = 0; i < lnum; i++) {          //find the ls value
      if (curL == ls[i]) {
        thisL = i;
      }
    }
    digitalWrite(pin[thisD][thisL], HIGH);    //activate the pin at the corresponding ds/ls value
  }
}
